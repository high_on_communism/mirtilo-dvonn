package graphics

import (
	"image/color"
	"math"
	"mirtilo-dvonn/internal/assets"
	"mirtilo-dvonn/internal/model"
	"strconv"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
)

const (
	ScreenWidth  = 1280
	ScreenHeight = 552
)

type Board struct {
	currentPlayerMove   model.PieceColor
	gameOver            bool
	rows                map[int8][]*Tile
	currentSelectedTile *Tile
	possibleDestTiles   []*Tile
	BoardImage          *ebiten.Image
	tileClickedChannel  chan [2]int8
	gameRestartChannel  chan struct{}
	paused              bool
}

func NewBoard(tileClickedChannel chan [2]int8, gameRestartChannel chan struct{}) *Board {
	b := &Board{
		currentPlayerMove:  model.White,
		gameOver:           false,
		tileClickedChannel: tileClickedChannel,
		gameRestartChannel: gameRestartChannel,
	}
	b.resetBoard()

	return b
}

func (b *Board) getTileAtPos(pos [2]float32) [2]int8 {
	var closestTile [2]int8

	minDist := math.Inf(1)

	for k := range b.rows {
		for i := range b.rows[k] {
			dist := b.rows[k][i].distanceToPoint(pos)
			if dist < minDist {
				minDist = dist
				closestTile = [2]int8{k, int8(i)}
			}
		}
	}

	if b.rows[closestTile[0]][closestTile[1]].isPointInside(pos) {
		return closestTile
	}

	return [2]int8{-1, -1}
}

func (b *Board) resetBoard() {
	b.currentPlayerMove = model.White
	b.currentSelectedTile = nil
	b.possibleDestTiles = []*Tile{}
	b.gameOver = false

	assets.GetAssetsManager().ResetBoardImage()
	b.BoardImage = assets.GetAssetsManager().BoardImage

	xAxisOffset := [...]float32{float32(HexWidth), float32(HexWidth) / 2, 0, float32(HexWidth) / 2, float32(HexWidth)}
	m := make(map[int8][]*Tile)
	m[0] = make([]*Tile, 9)
	m[1] = make([]*Tile, 10)
	m[2] = make([]*Tile, 11)
	m[3] = make([]*Tile, 10)
	m[4] = make([]*Tile, 9)

	for k := range m {
		for i := range m[k] {
			m[k][i] = NewTile(75+xAxisOffset[k]+float32(i*HexWidth), float32(80+98*int(k)))
		}
	}

	b.rows = m
}

func (b *Board) resetSelections() {
	for k := range b.rows {
		for i := range b.rows[k] {
			b.rows[k][i].isPossibleDest = false
			piece := b.rows[k][i].curPiece

			if piece != nil {
				piece.isSelected = false
			}
		}
	}
}

func (b *Board) pieceCount() (uint8, uint8) {
	var whiteCount, blackCount uint8

	for k := range b.rows {
		for i := range b.rows[k] {
			tile := b.rows[k][i]
			if tile.curPiece != nil {
				if tile.curPiece.stackColor == model.White {
					whiteCount += tile.curPiece.pieceCount
				} else if tile.curPiece.stackColor == model.Black {
					blackCount += tile.curPiece.pieceCount
				}
			}
		}
	}

	return whiteCount, blackCount
}

func (b *Board) PiecePlacedHandler(piecePlacedChannel <-chan model.PiecePlaced) {
	for piecePlaced := range piecePlacedChannel {
		b.rows[piecePlaced.Pos[0]][piecePlaced.Pos[1]].curPiece = NewPiece(piecePlaced.Color)
	}
}

func (b *Board) PieceSelectedHandler(pieceSelectedChannel <-chan model.PieceSelected) {
	for pieceSelected := range pieceSelectedChannel {
		b.rows[pieceSelected.Pos[0]][pieceSelected.Pos[1]].curPiece.isSelected = true
		for _, destPos := range pieceSelected.Dests {
			b.rows[destPos[0]][destPos[1]].isPossibleDest = true
		}
	}
}

func (b *Board) PieceUnselectedHandler(pieceUnselectedChannel <-chan struct{}) {
	for range pieceUnselectedChannel {
		b.resetSelections()
	}
}

func (b *Board) PieceMovedHandler(pieceMovedChannel <-chan model.PieceMoved) {
	for pieceMoved := range pieceMovedChannel {
		origTile := b.rows[pieceMoved.OriginPos[0]][pieceMoved.OriginPos[1]]
		destTile := b.rows[pieceMoved.DestPos[0]][pieceMoved.DestPos[1]]
		newPiece := destTile.curPiece.StackPiece(origTile.curPiece)
		destTile.curPiece = newPiece
		origTile.curPiece = nil
		b.resetSelections()
	}
}

func (b *Board) RemoveSectionsHandler(removeSectionsChannel <-chan model.PiecesToRemove) {
	for piecesToRemove := range removeSectionsChannel {
		for _, pos := range piecesToRemove {
			b.rows[pos[0]][pos[1]].curPiece = nil
		}
	}
}

func (b *Board) UpdateCurrentPlayerHandler(updateCurrentPlayer <-chan model.PieceColor) {
	for curPlayer := range updateCurrentPlayer {
		b.currentPlayerMove = curPlayer
	}
}

func (b *Board) GameOverHandler(gameOverChannel <-chan struct{}) {
	for range gameOverChannel {
		b.gameOver = true
	}
}

func (b *Board) GameRestartHandler(gameRestartChannel <-chan struct{}) {
	for range gameRestartChannel {
		b.resetBoard()
	}
}

func (b *Board) GamePausedHandler(gamePausedChannel <-chan struct{}) {
	for range gamePausedChannel {
		b.paused = !b.paused
	}
}

func (b *Board) ListenForInput(clickedSignal <-chan [2]int) {
	for pos := range clickedSignal {
		if b.gameOver {
			b.gameRestartChannel <- struct{}{}
			b.resetBoard()
		} else if !b.paused {
			clickedTile := b.getTileAtPos([2]float32{float32(pos[0]), float32(pos[1])})
			if clickedTile[0] >= 0 && clickedTile[1] >= 0 {
				b.tileClickedChannel <- clickedTile
			}
		}
	}
}

func (b *Board) Draw() {
	for k := range b.rows {
		for i := range b.rows[k] {
			b.rows[k][i].Draw(b.BoardImage)
		}
	}

	if b.gameOver {
		whiteCount, blackCount := b.pieceCount()
		winnerText := "It's a tie!"
		if whiteCount > blackCount {
			winnerText = "White wins!"
		} else if whiteCount < blackCount {
			winnerText = "Black wins!"
		}
		text.Draw(b.BoardImage,
			winnerText+"    "+strconv.Itoa(int(whiteCount))+" - "+strconv.Itoa(int(blackCount)),
			assets.GetAssetsManager().ResultFont, ScreenWidth/2-250, ScreenHeight/2,
			color.RGBA{R: 127, G: 127, B: 255, A: 255})
	} else {
		op := &ebiten.DrawImageOptions{}
		op.GeoM.Translate(float64(5), float64(5))
		b.BoardImage.DrawImage(assets.GetAssetsManager().CurrentPlayerIndicator[b.currentPlayerMove], op)
	}
}
