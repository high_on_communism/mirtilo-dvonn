package graphics

import (
	"github.com/hajimehoshi/ebiten/v2"
	"math"
	"mirtilo-dvonn/internal/assets"
	"mirtilo-dvonn/internal/model"
)

const (
	HexWidth int = 113
)

type Tile struct {
	curPiece       *Piece
	posX           float32
	posY           float32
	isPossibleDest bool
}

func NewTile(posX, posY float32) *Tile {
	return &Tile{
		curPiece:       nil,
		posX:           posX,
		posY:           posY,
		isPossibleDest: false,
	}
}

func (t *Tile) distanceToPoint(p [2]float32) float64 {
	return math.Sqrt(math.Pow(float64(t.posX-p[0]), 2) + math.Pow(float64(t.posY-p[1]), 2))
}

func (t *Tile) isPointInside(p [2]float32) bool {
	halfWidth := float32(HexWidth) / 2

	// check vertical bounds
	if p[0] < t.posX-halfWidth || p[0] > t.posX+halfWidth {
		return false
	}

	// check horizontal bounds
	if p[1] < t.posY-halfWidth || p[1] > t.posY+halfWidth {
		return false
	}

	// check diagonal bounds
	isLeftOfLine := func(l1, l2, p [2]float32) bool {
		// left considering the orientation from l1 to l2, meaning that if l1 is the leftmost point, this returns true
		// if point p is above that line
		return (l2[1]-l1[1])*(p[0]-l1[0]) > (l2[0]-l1[0])*(p[1]-l1[1])
	}
	quarterWidth := float32(HexWidth) / 4
	topLeft := [2]float32{t.posX - halfWidth, t.posY - quarterWidth}
	topRight := [2]float32{t.posX + halfWidth, t.posY - quarterWidth}
	botLeft := [2]float32{t.posX - halfWidth, t.posY + quarterWidth}
	botRight := [2]float32{t.posX + halfWidth, t.posY + quarterWidth}
	top := [2]float32{t.posX, t.posY - halfWidth}
	bot := [2]float32{t.posX, t.posY + halfWidth}

	if isLeftOfLine(topLeft, top, p) {
		return false
	}
	if isLeftOfLine(top, topRight, p) {
		return false
	}
	if isLeftOfLine(bot, botLeft, p) {
		return false
	}
	if isLeftOfLine(botRight, bot, p) {
		return false
	}

	return true
}

// Draw draws the current tile to the given boardImage.
func (t *Tile) Draw(boardImage *ebiten.Image) {
	imgPosX := t.posX - float32(HexWidth)/2.7
	imgPosY := t.posY - float32(HexWidth)/2.7
	if t.curPiece != nil {
		textPosX := t.posX - float32(HexWidth)/13
		textPosY := t.posY + float32(HexWidth)/8
		t.curPiece.Draw(boardImage, imgPosX, imgPosY, textPosX, textPosY, t.isPossibleDest)
	} else {
		op := &ebiten.DrawImageOptions{}
		op.GeoM.Translate(float64(imgPosX), float64(imgPosY))
		boardImage.DrawImage(assets.GetAssetsManager().PiecesImageMap[model.Empty], op)
	}
}
