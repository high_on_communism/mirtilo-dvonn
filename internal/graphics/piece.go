package graphics

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
	"image/color"
	"mirtilo-dvonn/internal/assets"
	"mirtilo-dvonn/internal/model"
	"strconv"
)

type Piece struct {
	pieceCount uint8
	stackColor model.PieceColor
	isPivot    bool
	isSelected bool
	pieceImage *ebiten.Image
}

func NewPiece(color model.PieceColor) *Piece {
	return &Piece{
		pieceCount: 1,
		stackColor: color,
		isPivot:    color == model.Red,
		isSelected: false,
		pieceImage: assets.GetAssetsManager().PiecesImageMap[color],
	}
}

func (p *Piece) StackPiece(newPiece *Piece) *Piece {
	return &Piece{
		pieceCount: p.pieceCount + newPiece.pieceCount,
		stackColor: newPiece.stackColor,
		isPivot:    p.isPivot || newPiece.isPivot,
		isSelected: false,
		pieceImage: assets.GetAssetsManager().PiecesImageMap[newPiece.stackColor],
	}
}

func (p *Piece) Draw(boardImage *ebiten.Image, imgPosX, imgPosY, textPosX, textPosY float32, isPossibleDest bool) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(float64(imgPosX), float64(imgPosY))
	if p.isSelected {
		boardImage.DrawImage(assets.GetAssetsManager().PiecesSelectedImageMap[p.stackColor], op)
	} else if isPossibleDest {
		boardImage.DrawImage(assets.GetAssetsManager().PiecesDestImageMap[p.stackColor], op)
	} else {
		boardImage.DrawImage(p.pieceImage, op)
	}

	channelValue := uint8(255)
	if p.stackColor == model.Black {
		channelValue = 0
	}

	textColor := color.RGBA{R: channelValue, G: channelValue, B: channelValue, A: 255}
	if p.isPivot {
		textColor = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	}

	text.Draw(boardImage,
		strconv.Itoa(int(p.pieceCount)), assets.GetAssetsManager().StackSizeFont, int(textPosX), int(textPosY), textColor)
}
