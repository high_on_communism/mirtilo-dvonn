package gamelogic

import (
	"mirtilo-dvonn/internal/model"
)

type Piece struct {
	pieceCount uint8
	stackColor model.PieceColor
	isPivot    bool
	isSelected bool
}

func NewPiece(color model.PieceColor) *Piece {
	return &Piece{
		pieceCount: 1,
		stackColor: color,
		isPivot:    color == model.Red,
		isSelected: false,
	}
}

func (p *Piece) StackPiece(newPiece *Piece) *Piece {
	return &Piece{
		pieceCount: p.pieceCount + newPiece.pieceCount,
		stackColor: newPiece.stackColor,
		isPivot:    p.isPivot || newPiece.isPivot,
		isSelected: false,
	}
}
