package gamelogic

import (
	"mirtilo-dvonn/internal/model"
)

type Dir uint8

const (
	Left Dir = iota
	TopLeft
	TopRight
	Right
	BotRight
	BotLeft
)

// get the offset for each dest row according to direction moved
var destRowOffset = map[int8]map[Dir]int8{
	0: {
		Left:     -1,
		TopLeft:  -1,
		TopRight: 0,
		Right:    1,
	},
	1: {
		Left:     -1,
		TopLeft:  -1,
		TopRight: 0,
		Right:    1,
		BotLeft:  0,
		BotRight: 1,
	},
	2: {
		Left:     -1,
		TopLeft:  0,
		TopRight: 1,
		Right:    1,
		BotLeft:  0,
		BotRight: 1,
	},
	3: {
		Left:     -1,
		TopLeft:  0,
		TopRight: 1,
		Right:    1,
		BotLeft:  -1,
		BotRight: 0,
	},
	4: {
		Left:     -1,
		Right:    1,
		BotLeft:  -1,
		BotRight: 0,
	},
}

type Board struct {
	Rows                       map[int8][]*Tile
	CountPieces                uint8
	currentSelectedTile        *Tile
	possibleDestTiles          []*Tile
	boardHasBeenFilled         bool
	currentPlayerMove          model.PieceColor
	gameOver                   bool
	piecePlacedChannel         chan model.PiecePlaced
	pieceSelectedChannel       chan model.PieceSelected
	pieceUnselectedChannel     chan struct{}
	pieceMovedChannel          chan model.PieceMoved
	removeSectionsChannel      chan model.PiecesToRemove
	updateCurrentPlayerChannel chan model.PieceColor
	gameOverChannel            chan struct{}
	PauseChannel               chan struct{}
	aiPlayChannels             [2]chan struct{}
}

func (b *Board) BoardHasBeenFilled() bool {
	return b.boardHasBeenFilled
}

func NewBoard(
	piecePlacedChannel chan model.PiecePlaced,
	pieceSelectedChannel chan model.PieceSelected,
	pieceUnselectedChannel chan struct{},
	pieceMovedChannel chan model.PieceMoved,
	removeSectionsChannel chan model.PiecesToRemove,
	updateCurrentPlayerChannel chan model.PieceColor,
	gameOverChannel chan struct{},
	pauseChannel chan struct{}) *Board {
	b := &Board{
		piecePlacedChannel:         piecePlacedChannel,
		pieceSelectedChannel:       pieceSelectedChannel,
		pieceUnselectedChannel:     pieceUnselectedChannel,
		pieceMovedChannel:          pieceMovedChannel,
		removeSectionsChannel:      removeSectionsChannel,
		updateCurrentPlayerChannel: updateCurrentPlayerChannel,
		gameOverChannel:            gameOverChannel,
		PauseChannel:               pauseChannel,
	}

	b.resetBoard()

	return b
}

func (b *Board) SetAIChannels(aiChannels [2]chan struct{}) {
	b.aiPlayChannels = aiChannels
}

func (b *Board) resetBoard() {
	b.CountPieces = 0
	b.boardHasBeenFilled = false
	b.currentSelectedTile = nil
	b.possibleDestTiles = []*Tile{}
	b.currentPlayerMove = model.White
	b.gameOver = false

	m := make(map[int8][]*Tile)
	m[0] = make([]*Tile, 9)
	m[1] = make([]*Tile, 10)
	m[2] = make([]*Tile, 11)
	m[3] = make([]*Tile, 10)
	m[4] = make([]*Tile, 9)

	for k := range m {
		for i := range m[k] {
			m[k][i] = NewTile(k, int8(i))
		}
	}

	b.Rows = m
}

func (b *Board) toggleCurrentPlayer() {
	if b.currentPlayerMove == model.White {
		b.currentPlayerMove = model.Black
		if b.aiPlayChannels[1] != nil {
			b.aiPlayChannels[1] <- struct{}{}
		}
	} else {
		b.currentPlayerMove = model.White
		if b.aiPlayChannels[0] != nil {
			b.aiPlayChannels[0] <- struct{}{}
		}
	}
	b.updateCurrentPlayerChannel <- b.currentPlayerMove
}

func (b *Board) TileClickedHandler(tileClickedChannel <-chan [2]int8) {
	for idxs := range tileClickedChannel {
		t := b.Rows[idxs[0]][idxs[1]]
		if b.boardHasBeenFilled {
			b.tileClickedStackingPieces(t)
		} else {
			b.TileClickedFillingBoard(t)
		}
	}
}

func (b *Board) GameRestartHandler(gameRestartChannel <-chan struct{}) {
	for range gameRestartChannel {
		b.resetBoard()
		b.updateCurrentPlayerChannel <- b.currentPlayerMove
	}
}

func (b *Board) TileClickedFillingBoard(t *Tile) {
	if t.CurPiece != nil { // if tile clicked already has a piece, discard this click
		return
	}

	if b.CountPieces < 3 {
		t.CurPiece = NewPiece(model.Red)
	} else if b.CountPieces%2 == 0 {
		t.CurPiece = NewPiece(model.White)
	} else {
		t.CurPiece = NewPiece(model.Black)
	}
	b.piecePlacedChannel <- model.PiecePlaced{Pos: [2]int8{t.rowIdx, t.colIdx}, Color: t.CurPiece.stackColor}
	b.CountPieces++

	if b.CountPieces == 49 {
		b.boardHasBeenFilled = true
		b.currentPlayerMove = model.White
		b.updateCurrentPlayerChannel <- b.currentPlayerMove
		if b.aiPlayChannels[0] != nil {
			b.aiPlayChannels[0] <- struct{}{}
		}
	} else {
		b.toggleCurrentPlayer()
	}
}

func (b *Board) tileClickedStackingPieces(t *Tile) {
	if t.CurPiece == nil || (b.currentSelectedTile == nil && t.CurPiece.stackColor == model.Red) {
		return // can't select an empty tile or a neutral piece
	}

	switch {
	case b.currentSelectedTile == nil:
		b.selectTile(t)
	case b.currentSelectedTile == t:
		b.unselectTile()
	case b.canMovePieceToTile(t):
		b.MoveToTile(b.currentSelectedTile, t)
	}
}

func (b *Board) GetNeighbourTiles(curTile *Tile, pred func(*Tile) bool) []*Tile {
	var neighbours []*Tile

	dirs := [6]Dir{Left, TopLeft, TopRight, Right, BotLeft, BotRight}

	for _, dir := range dirs {
		destTile := b.walkInDir(curTile, dir)
		if destTile != nil && pred(destTile) {
			neighbours = append(neighbours, destTile)
		}
	}

	return neighbours
}

func (b *Board) selectTile(t *Tile) {
	if t.CurPiece.stackColor == b.currentPlayerMove {
		isFree := len(b.GetNeighbourTiles(t, func(testTile *Tile) bool {
			return testTile.CurPiece != nil
		})) < 6
		if isFree {
			b.currentSelectedTile = t
			b.currentSelectedTile.CurPiece.isSelected = true
			destTiles := b.getPossibleMoves(b.currentSelectedTile)

			for _, destTile := range destTiles {
				destTile.isPossibleDest = true
			}

			b.possibleDestTiles = destTiles

			var possibleDests [][2]int8
			for _, destTile := range b.possibleDestTiles {
				possibleDests = append(possibleDests, [2]int8{destTile.rowIdx, destTile.colIdx})
			}
			b.pieceSelectedChannel <- model.PieceSelected{Pos: [2]int8{t.rowIdx, t.colIdx}, Dests: possibleDests}
		}
	}
}

func (b *Board) unselectTile() {
	b.currentSelectedTile.CurPiece.isSelected = false
	b.currentSelectedTile = nil
	b.resetPossibleMoves()
	b.pieceUnselectedChannel <- struct{}{}
}

func (b *Board) getPossibleMoves(curTile *Tile) []*Tile {
	var possibleDestTiles []*Tile
	if curTile == nil { // dumb sanity check but still
		return possibleDestTiles
	}

	curPiece := curTile.CurPiece
	jumpDist := int8(curPiece.pieceCount)

	// horizontal jumps are trivial, do a simple index check instead
	if curTile.colIdx-jumpDist >= 0 && b.Rows[curTile.rowIdx][curTile.colIdx-jumpDist].CurPiece != nil {
		possibleDestTiles = append(possibleDestTiles, b.Rows[curTile.rowIdx][curTile.colIdx-jumpDist])
	}

	if curTile.colIdx+jumpDist < int8(len(b.Rows[curTile.rowIdx])) &&
		b.Rows[curTile.rowIdx][curTile.colIdx+jumpDist].CurPiece != nil {
		possibleDestTiles = append(possibleDestTiles, b.Rows[curTile.rowIdx][curTile.colIdx+jumpDist])
	}

	// diagonal jumps can be optimized out if jump distance is higher than 4 (there are no possible 5+ diagonal jumps)
	if jumpDist < 5 {
		dirs := [4]Dir{TopLeft, TopRight, BotLeft, BotRight}
		for _, dir := range dirs {
			i := int8(0)
			destTile := curTile

			for ; i < jumpDist; i++ {
				destTile = b.walkInDir(destTile, dir)
				if destTile == nil {
					break
				}
			}

			if destTile != nil && destTile.CurPiece != nil {
				possibleDestTiles = append(possibleDestTiles, destTile)
			}
		}
	}

	return possibleDestTiles
}

func (b *Board) resetPossibleMoves() {
	for _, t := range b.possibleDestTiles {
		t.isPossibleDest = false
	}

	b.possibleDestTiles = []*Tile{}
}

func (b *Board) walkInDir(t *Tile, dir Dir) *Tile {
	row := t.rowIdx
	col := t.colIdx
	destRow := row

	if dir == TopLeft || dir == TopRight {
		destRow = row - 1
	} else if dir == BotLeft || dir == BotRight {
		destRow = row + 1
	}

	if destRow < 0 || int(destRow) >= len(b.Rows) {
		return nil
	}

	destCol := col + destRowOffset[destRow][dir]
	if destCol < 0 || int(destCol) >= len(b.Rows[destRow]) {
		return nil
	}

	return b.Rows[destRow][int(destCol)]
}

func (b *Board) canMovePieceToTile(t *Tile) bool {
	for _, destTile := range b.possibleDestTiles {
		if t == destTile {
			return true
		}
	}

	return false
}

func (b *Board) MoveToTile(origTile *Tile, destTile *Tile) {
	destTile.CurPiece = destTile.CurPiece.StackPiece(origTile.CurPiece)
	b.currentSelectedTile.CurPiece = nil
	b.currentSelectedTile = nil
	b.toggleCurrentPlayer()
	b.resetPossibleMoves()

	b.CountPieces--
	b.pieceMovedChannel <- model.PieceMoved{
		OriginPos: [2]int8{origTile.rowIdx, origTile.colIdx},
		DestPos:   [2]int8{destTile.rowIdx, destTile.colIdx},
	}

	b.removeCutSections(origTile)

	b.checkMoves()
}

func (b *Board) removeCutSections(removedPieceTile *Tile) {
	var neighbours []*Tile

	dirs := [6]Dir{Left, TopLeft, TopRight, Right, BotLeft, BotRight}
	for _, dir := range dirs {
		destTile := b.walkInDir(removedPieceTile, dir)
		if destTile != nil && destTile.CurPiece != nil {
			neighbours = append(neighbours, destTile)
		}
	}

	sections := map[*Tile]map[*Tile]struct{}{}
	sameSectionFlags := make([]bool, 4)

	for i, n := range neighbours {
		if i > 0 { // ignore neighbour if it has been reached trough one of the previous neighbours
			if _, ok := sections[neighbours[i-1]][n]; ok {
				sameSectionFlags = append(sameSectionFlags, true)
				sections[n] = sections[neighbours[i-1]]

				continue
			}
		}

		sameSectionFlags = append(sameSectionFlags, false)

		section := map[*Tile]struct{}{}
		sections[n] = section
		b.floodFillFromTile(n, section)
	}

	isPivotSection := func(section map[*Tile]struct{}) bool {
		for t := range section {
			//this sanity check shouldn't be necessary, so we won't use it because it helped find some bugs already
			//it "fixes" most bugs regarding it, but we should aim to actually fix the underlying bug and not just cover it up
			if t != nil && t.CurPiece != nil && t.CurPiece.isPivot {
				//if t.curPiece.isPivot {
				return true
			}
		}

		return false
	}

	removedPositions := model.PiecesToRemove{}

	sectionIdx := 0
	for _, section := range sections {
		if sectionIdx > 0 && sameSectionFlags[sectionIdx-1] {
			sectionIdx++

			continue
		}

		if !isPivotSection(section) {
			for t := range section {
				t.CurPiece = nil
				removedPositions = append(removedPositions, [2]int8{t.rowIdx, t.colIdx})
				b.CountPieces--
			}
		}
		sectionIdx++
	}

	b.removeSectionsChannel <- removedPositions
}

func (b *Board) floodFillFromTile(t *Tile, visited map[*Tile]struct{}) {
	visited[t] = struct{}{}
	neighbours := b.GetNeighbourTiles(t, func(testTile *Tile) bool {
		if testTile.CurPiece == nil {
			return false
		}
		_, ok := visited[testTile]

		return !ok
	})

	for _, n := range neighbours {
		if _, ok := visited[n]; ok {
			continue // ignore neighbour if it has been reached trough one of the previous neighbours
		}

		b.floodFillFromTile(n, visited)
	}
}

func (b *Board) checkMoves() {
	var whiteHasMovesLeft, blackHasMovesLeft bool
foundMoves:
	for k := range b.Rows {
		for i := range b.Rows[k] {
			tile := b.Rows[k][i]

			if tile.CurPiece != nil {
				if !whiteHasMovesLeft && tile.CurPiece.stackColor == model.White {
					if len(b.getPossibleMoves(tile)) > 0 {
						whiteHasMovesLeft = true
					}
				} else if !blackHasMovesLeft && tile.CurPiece.stackColor == model.Black {
					if len(b.getPossibleMoves(tile)) > 0 {
						blackHasMovesLeft = true
					}
				}
			}
			if whiteHasMovesLeft && blackHasMovesLeft {
				break foundMoves
			}
		}
	}

	if !whiteHasMovesLeft && !blackHasMovesLeft {
		b.gameOver = true
		b.gameOverChannel <- struct{}{}
	} else {
		if whiteHasMovesLeft && !blackHasMovesLeft {
			b.currentPlayerMove = model.White
		} else if !whiteHasMovesLeft && blackHasMovesLeft {
			b.currentPlayerMove = model.Black
		}
		b.updateCurrentPlayerChannel <- b.currentPlayerMove
	}
}
