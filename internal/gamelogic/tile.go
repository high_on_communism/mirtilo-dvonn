package gamelogic

type Tile struct {
	CurPiece       *Piece
	rowIdx         int8
	colIdx         int8
	isPossibleDest bool
}

func NewTile(rowIdx, colIdx int8) *Tile {
	return &Tile{
		CurPiece:       nil,
		rowIdx:         rowIdx,
		colIdx:         colIdx,
		isPossibleDest: false,
	}
}
