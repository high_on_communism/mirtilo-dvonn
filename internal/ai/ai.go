package ai

import (
	"mirtilo-dvonn/internal/gamelogic"
	"time"
)

func NewAI(board *gamelogic.Board, strategyName string) (*AI, error) {
	strategy, err := getStrategy(strategyName)
	if err != nil {
		return nil, err
	}
	return &AI{
		curBoard: board,
		strategy: strategy,
	}, nil
}

func (ai *AI) NextMoveHandler(getNextMoveChannel <-chan struct{}, waitTime time.Duration) {
	for range getNextMoveChannel {
		ai.curBoard.PauseChannel <- struct{}{}
		time.Sleep(waitTime * time.Second)
		if ai.curBoard.BoardHasBeenFilled() {
			move := ai.strategy.StackingStrategy.GetBestMove(*ai.curBoard)
			ai.curBoard.MoveToTile(ai.curBoard.Rows[move[0][0]][move[0][1]], ai.curBoard.Rows[move[1][0]][move[1][1]])
		} else {
			move := [2]TilePos{ai.strategy.FillingStrategy.GetBestTile(*ai.curBoard), {-1, -1}}
			ai.curBoard.TileClickedFillingBoard(ai.curBoard.Rows[move[0][0]][move[0][1]])
		}
		ai.curBoard.PauseChannel <- struct{}{}
	}
}
