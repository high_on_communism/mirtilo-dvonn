package ai

import (
	"mirtilo-dvonn/internal/gamelogic"
)

type TilePos [2]int8

// Move first array is origin TilePos, second is destination TilePos,
// if it's a piece placed then the destination TilePos is set to [-1, -1]
type Move [2]TilePos

type AI struct {
	curBoard *gamelogic.Board
	strategy strategy
}

type fillingStrategy struct {
	Name        string
	GetBestTile func(curBoard gamelogic.Board) TilePos
}

type stackingStrategy struct {
	Name        string
	GetBestMove func(curBoard gamelogic.Board) Move
}

type strategy struct {
	Name             string
	FillingStrategy  fillingStrategy
	StackingStrategy stackingStrategy
}
