package ai

import (
	"math"
	"math/rand"
	"mirtilo-dvonn/internal/gamelogic"
	"time"
)

var fillingStrategyBasic_1 = fillingStrategy{
	Name: "Basic_1",
	GetBestTile: func(curBoard gamelogic.Board) TilePos {
		return getNextMoveFillingBoard(curBoard)
	},
}

func getNextMoveFillingBoard(curBoard gamelogic.Board) TilePos {

	// TODO: look at logic for red pieces position, for now just return random positions for red pieces
	if curBoard.CountPieces < 3 {
		for {
			randomGen := rand.New(rand.NewSource(time.Now().UnixNano()))

			randomRow := int8(randomGen.Intn(5))
			randomColumn := int8(randomGen.Intn(len(curBoard.Rows[randomRow])))

			tile := curBoard.Rows[randomRow][randomColumn]
			if tile != nil && tile.CurPiece == nil {
				return [2]int8{randomRow, randomColumn}
			}
		}
	}

	maxScore := int32(math.Inf(-1))
	var maxScoreTile TilePos = [2]int8{-1, -1}

	// for now just get random empty TilePos
	for row := range curBoard.Rows {
		for col := range curBoard.Rows[row] {
			tile := curBoard.Rows[row][col]
			if tile != nil && tile.CurPiece == nil {
				var tilePos TilePos = [2]int8{row, int8(col)}
				score := scoreTilePosFillingBoard(curBoard, tilePos)
				if score > maxScore {
					maxScore = score
					maxScoreTile = tilePos
				}
			}
		}
	}

	return maxScoreTile
}

func scoreTilePosFillingBoard(curBoard gamelogic.Board, tilePos TilePos) int32 {

	var score int32 = 0

	tile := curBoard.Rows[tilePos[0]][tilePos[1]]
	numNeighbours := int32(len(curBoard.GetNeighbourTiles(tile, func(tile *gamelogic.Tile) bool {
		return true
	})))
	score += 6 - numNeighbours // number of open positions increases the score for that tile position

	return score
}
