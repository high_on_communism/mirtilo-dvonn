package ai

import "errors"

var strategies map[string]strategy = nil
var mutex = make(chan struct{}, 1)

func getStrategy(strategyName string) (strategy, error) {
	mutex <- struct{}{} // ensure that singleton access is safe
	defer func() {
		<-mutex
	}()
	if strategies == nil {
		fillStrategies()
	}
	strat, ok := strategies[strategyName]
	if !ok {
		return strategy{}, errors.New("Strategy \"" + strategyName + "\" not found.")
	}
	return strat, nil
}

func fillStrategies() {
	strategies = make(map[string]strategy)
	strategyBasic_1 := strategy{
		Name:            "basic_1",
		FillingStrategy: fillingStrategyBasic_1,
		// StackingStrategy: ,
	}
	strategies[strategyBasic_1.Name] = strategyBasic_1
}
