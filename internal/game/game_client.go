package game

import (
	"mirtilo-dvonn/internal/graphics"
	"mirtilo-dvonn/internal/input"
	"mirtilo-dvonn/internal/model"
)

func NewGameClient() *Game {
	logicChannels := LogicChannels{
		PiecePlacedChannel:         make(chan model.PiecePlaced),
		PieceSelectedChannel:       make(chan model.PieceSelected),
		PieceUnselectedChannel:     make(chan struct{}),
		PieceMovedChannel:          make(chan model.PieceMoved),
		RemoveSectionsChannel:      make(chan model.PiecesToRemove),
		UpdateCurrentPlayerChannel: make(chan model.PieceColor),
		GameOverChannel:            make(chan struct{}),
		GameRestartChannel:         make(chan struct{}),
	}

	uiChannels := UIChannels{
		TileClickedChannel: make(chan [2]int8),
		GameRestartChannel: make(chan struct{}),
	}

	return initGameClient(logicChannels, uiChannels)
}

func initGameClient(logicChannels LogicChannels, uiChannels UIChannels) *Game {
	uiBoard := graphics.NewBoard(uiChannels.TileClickedChannel, uiChannels.GameRestartChannel)

	inputChannel := make(chan [2]int)

	go uiBoard.ListenForInput(inputChannel)

	go uiBoard.PiecePlacedHandler(logicChannels.PiecePlacedChannel)
	go uiBoard.PieceSelectedHandler(logicChannels.PieceSelectedChannel)
	go uiBoard.PieceUnselectedHandler(logicChannels.PieceUnselectedChannel)
	go uiBoard.PieceMovedHandler(logicChannels.PieceMovedChannel)
	go uiBoard.RemoveSectionsHandler(logicChannels.RemoveSectionsChannel)
	go uiBoard.UpdateCurrentPlayerHandler(logicChannels.UpdateCurrentPlayerChannel)
	go uiBoard.GameOverHandler(logicChannels.GameOverChannel)
	go uiBoard.GameRestartHandler(logicChannels.GameRestartChannel)
	go uiBoard.GamePausedHandler(logicChannels.GamePausedChannel)

	return &Game{
		input:         input.NewInput(),
		inputChannel:  inputChannel,
		uiBoard:       uiBoard,
		LogicChannels: &logicChannels,
		UIChannels:    &uiChannels,
	}
}
