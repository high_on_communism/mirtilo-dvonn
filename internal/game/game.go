package game

import (
	"image/color"
	"mirtilo-dvonn/internal/gamelogic"
	"mirtilo-dvonn/internal/graphics"
	"mirtilo-dvonn/internal/input"
	"mirtilo-dvonn/internal/model"

	"github.com/hajimehoshi/ebiten/v2"
)

type LogicChannels struct {
	PiecePlacedChannel         chan model.PiecePlaced
	PieceSelectedChannel       chan model.PieceSelected
	PieceUnselectedChannel     chan struct{}
	PieceMovedChannel          chan model.PieceMoved
	RemoveSectionsChannel      chan model.PiecesToRemove
	UpdateCurrentPlayerChannel chan model.PieceColor
	GameOverChannel            chan struct{}
	GameRestartChannel         chan struct{}
	GamePausedChannel          chan struct{}
}

type UIChannels struct {
	TileClickedChannel chan [2]int8
	GameRestartChannel chan struct{}
}

type Game struct {
	input         *input.Input
	board         *gamelogic.Board
	uiBoard       *graphics.Board
	inputChannel  chan [2]int
	LogicChannels *LogicChannels
	UIChannels    *UIChannels
}

func (g *Game) Layout(_, _ int) (screenWidth, screenHeight int) {
	return graphics.ScreenWidth, graphics.ScreenHeight
}

func (g *Game) Update() error {
	if g.inputChannel != nil {
		g.input.Update(g.inputChannel)
	}

	return nil // no error is ever returned, but we need to conform to the ebiten game interface
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.RGBA{R: 100, G: 20, B: 255, A: 255})
	g.uiBoard.Draw()
	screen.DrawImage(g.uiBoard.BoardImage, &ebiten.DrawImageOptions{})
}
