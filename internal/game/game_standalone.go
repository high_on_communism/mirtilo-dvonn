package game

import (
	"encoding/json"
	"io"
	"log"
	"mirtilo-dvonn/internal/ai"
	"mirtilo-dvonn/internal/gamelogic"
	"mirtilo-dvonn/internal/graphics"
	"mirtilo-dvonn/internal/input"
	"mirtilo-dvonn/internal/model"
	"os"
	"time"
)

type Config struct {
	Player1              string        `json:"player1"`
	Player2              string        `json:"player2"`
	WaitTimeBetweenPlays time.Duration `json:"wait_time_between_plays"`
}

func NewGameStandalone() *Game {

	config := readConfig()

	player1IsHuman := config.Player1 == "human"
	player2IsHuman := config.Player2 == "human"
	waitTimeBetweenPlays := config.WaitTimeBetweenPlays

	logicChannels := LogicChannels{
		PiecePlacedChannel:         make(chan model.PiecePlaced),
		PieceSelectedChannel:       make(chan model.PieceSelected),
		PieceUnselectedChannel:     make(chan struct{}),
		PieceMovedChannel:          make(chan model.PieceMoved),
		RemoveSectionsChannel:      make(chan model.PiecesToRemove),
		UpdateCurrentPlayerChannel: make(chan model.PieceColor),
		GameOverChannel:            make(chan struct{}),
		GameRestartChannel:         make(chan struct{}),
		GamePausedChannel:          make(chan struct{}),
	}

	uiChannels := UIChannels{
		TileClickedChannel: make(chan [2]int8),
		GameRestartChannel: make(chan struct{}),
	}

	listenForInput := player1IsHuman || player2IsHuman
	game := initGameStandalone(logicChannels, uiChannels, listenForInput)

	aiChannels := [2]chan struct{}{nil, nil}
	for playerIdx, playerIsHuman := range [2]bool{player1IsHuman, player2IsHuman} {
		if !playerIsHuman {
			var stratName string
			if playerIdx == 0 {
				stratName = config.Player1
			} else {
				stratName = config.Player2
			}
			aiChannels[playerIdx] = make(chan struct{}, 1)
			aiPlayer, err := ai.NewAI(game.board, stratName)
			if err != nil {
				panic(err.Error())
			}
			go aiPlayer.NextMoveHandler(aiChannels[playerIdx], waitTimeBetweenPlays)
		}
	}

	game.board.SetAIChannels(aiChannels)

	if !player1IsHuman {
		aiChannels[0] <- struct{}{}
	}

	return game
}

func readConfig() Config {
	jsonFile, err := os.Open("config/config.json")
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	configBytes, err := io.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	var config Config
	json.Unmarshal(configBytes, &config)

	return config
}

func initGameStandalone(logicChannels LogicChannels, uiChannels UIChannels, listenForInput bool) *Game {

	board := gamelogic.NewBoard(
		logicChannels.PiecePlacedChannel,
		logicChannels.PieceSelectedChannel,
		logicChannels.PieceUnselectedChannel,
		logicChannels.PieceMovedChannel,
		logicChannels.RemoveSectionsChannel,
		logicChannels.UpdateCurrentPlayerChannel,
		logicChannels.GameOverChannel,
		logicChannels.GamePausedChannel)

	go board.TileClickedHandler(uiChannels.TileClickedChannel)
	go board.GameRestartHandler(uiChannels.GameRestartChannel)

	uiBoard := graphics.NewBoard(uiChannels.TileClickedChannel, uiChannels.GameRestartChannel)
	go uiBoard.PiecePlacedHandler(logicChannels.PiecePlacedChannel)
	go uiBoard.PieceSelectedHandler(logicChannels.PieceSelectedChannel)
	go uiBoard.PieceUnselectedHandler(logicChannels.PieceUnselectedChannel)
	go uiBoard.PieceMovedHandler(logicChannels.PieceMovedChannel)
	go uiBoard.RemoveSectionsHandler(logicChannels.RemoveSectionsChannel)
	go uiBoard.UpdateCurrentPlayerHandler(logicChannels.UpdateCurrentPlayerChannel)
	go uiBoard.GameOverHandler(logicChannels.GameOverChannel)
	go uiBoard.GameRestartHandler(logicChannels.GameRestartChannel)
	go uiBoard.GamePausedHandler(logicChannels.GamePausedChannel)

	var inputChannel chan [2]int = nil

	if listenForInput {
		inputChannel = make(chan [2]int)
		go uiBoard.ListenForInput(inputChannel)
	}

	return &Game{
		input:         input.NewInput(),
		inputChannel:  inputChannel,
		board:         board,
		uiBoard:       uiBoard,
		LogicChannels: &logicChannels,
		UIChannels:    &uiChannels,
	}
}
