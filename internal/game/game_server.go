package game

import (
	"mirtilo-dvonn/internal/gamelogic"
	"mirtilo-dvonn/internal/model"
)

func NewGameServer() *Game {
	logicChannels := LogicChannels{
		PiecePlacedChannel:         make(chan model.PiecePlaced),
		PieceSelectedChannel:       make(chan model.PieceSelected),
		PieceUnselectedChannel:     make(chan struct{}),
		PieceMovedChannel:          make(chan model.PieceMoved),
		RemoveSectionsChannel:      make(chan model.PiecesToRemove),
		UpdateCurrentPlayerChannel: make(chan model.PieceColor),
		GameOverChannel:            make(chan struct{}),
		GameRestartChannel:         make(chan struct{}),
		GamePausedChannel:          make(chan struct{}),
	}

	uiChannels := UIChannels{
		TileClickedChannel: make(chan [2]int8),
		GameRestartChannel: make(chan struct{}),
	}

	return initGameServer(logicChannels, uiChannels)
}

func initGameServer(logicChannels LogicChannels, uiChannels UIChannels) *Game {
	board := gamelogic.NewBoard(
		logicChannels.PiecePlacedChannel,
		logicChannels.PieceSelectedChannel,
		logicChannels.PieceUnselectedChannel,
		logicChannels.PieceMovedChannel,
		logicChannels.RemoveSectionsChannel,
		logicChannels.UpdateCurrentPlayerChannel,
		logicChannels.GameOverChannel,
		logicChannels.GamePausedChannel)

	go board.TileClickedHandler(uiChannels.TileClickedChannel)
	go board.GameRestartHandler(uiChannels.GameRestartChannel)

	return &Game{
		board:         board,
		LogicChannels: &logicChannels,
		UIChannels:    &uiChannels,
	}
}
