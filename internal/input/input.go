package input

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type mouseState int

const (
	mouseStateNone mouseState = iota
	mouseStatePressing
)

type Input struct {
	mouseState mouseState
}

func NewInput() *Input {
	return &Input{}
}

func (input *Input) Update(clickedSignal chan<- [2]int) {
	switch input.mouseState {
	case mouseStateNone:
		if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			input.mouseState = mouseStatePressing
		}
	case mouseStatePressing:
		if !ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			x, y := ebiten.CursorPosition()
			clickedSignal <- [2]int{x, y}
			input.mouseState = mouseStateNone
		}
	}
}
