package protocol

const (
	ConnHost = "localhost"
	ConnPort = "3333"
	ConnType = "tcp"
)

const (
	PiecePlaced byte = iota
	UpdatePlayer
	PieceSelected
	PieceUnselected
	PieceMoved
	RemovePieces
	GameOver
	GameRestart
	GamePaused
)

const (
	ColorAssignment byte = iota
	TileClicked
	GameRestartClicked
)
