package model

type PieceColor int8

const (
	Empty PieceColor = iota
	Red
	White
	Black
)
