package model

type PieceMoved struct {
	OriginPos [2]int8
	DestPos   [2]int8
}
