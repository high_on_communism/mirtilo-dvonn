package model

type PieceSelected struct {
	Pos   [2]int8
	Dests [][2]int8
}
