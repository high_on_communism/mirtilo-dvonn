package model

type PiecePlaced struct {
	Pos   [2]int8
	Color PieceColor
}
