package assets

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"log"
	"mirtilo-dvonn/assets/fonts"
	. "mirtilo-dvonn/internal/model"
)

// Manager assets manager exists to ensure that we keep all assets in one place and avoid duplicate assets when
// creating new pieces, this way the same images can be reused for different pieces
type Manager struct {
	BoardImage             *ebiten.Image
	PiecesImageMap         map[PieceColor]*ebiten.Image
	PiecesSelectedImageMap map[PieceColor]*ebiten.Image
	PiecesDestImageMap     map[PieceColor]*ebiten.Image
	CurrentPlayerIndicator map[PieceColor]*ebiten.Image
	StackSizeFont          font.Face
	ResultFont             font.Face
}

var assetsManager *Manager = nil
var mutex = make(chan struct{}, 1)

func GetAssetsManager() *Manager {
	mutex <- struct{}{} // ensure that singleton access is safe
	defer func() {
		<-mutex
	}()
	if assetsManager == nil {
		buildAssetsManager()
	}
	return assetsManager
}

func buildAssetsManager() {
	colorMap := map[PieceColor]string{
		Empty: "empty",
		Red:   "red",
		White: "white",
		Black: "black",
	}
	piecesImageMap := make(map[PieceColor]*ebiten.Image, 4)
	for k := range colorMap {
		circleImage, _, err := ebitenutil.NewImageFromFile("assets/images/" + colorMap[k] + "-circle.png")
		if err != nil {
			log.Fatal(err)
		}
		piecesImageMap[k] = circleImage
	}

	selectedPiecesImageMap := make(map[PieceColor]*ebiten.Image, 2)
	for _, v := range [2]PieceColor{White, Black} {
		circleImage, _, err := ebitenutil.NewImageFromFile("assets/images/" + colorMap[v] + "-circle-selected.png")
		if err != nil {
			log.Fatal(err)
		}
		selectedPiecesImageMap[v] = circleImage
	}

	possibleDestImageMap := make(map[PieceColor]*ebiten.Image, 2)
	for _, v := range [3]PieceColor{Red, White, Black} {
		destPiece, _, err := ebitenutil.NewImageFromFile("assets/images/" + colorMap[v] + "-circle-dest.png")
		if err != nil {
			log.Fatal(err)
		}
		possibleDestImageMap[v] = destPiece
	}

	currentPlayerIndicatorImageMap := make(map[PieceColor]*ebiten.Image, 2)
	for _, v := range [2]PieceColor{White, Black} {
		indicator, _, err := ebitenutil.NewImageFromFile("assets/images/current-player-indicator-" + colorMap[v] + ".png")
		if err != nil {
			log.Fatal(err)
		}

		currentPlayerIndicatorImageMap[v] = indicator
	}

	boardImage, _, err := ebitenutil.NewImageFromFile("assets/images/DVONN-board.png")
	if err != nil {
		log.Fatal(err)
	}

	tt, err := opentype.Parse(fonts.MPlus1pRegularTTF)
	if err != nil {
		log.Fatal(err)
	}
	const dpi = 72
	stackSizeFont, err := opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    32,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
	if err != nil {
		log.Fatal(err)
	}

	resultFont, err := opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    56,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
	if err != nil {
		log.Fatal(err)
	}

	assetsManager = &Manager{
		BoardImage:             boardImage,
		PiecesImageMap:         piecesImageMap,
		PiecesSelectedImageMap: selectedPiecesImageMap,
		PiecesDestImageMap:     possibleDestImageMap,
		CurrentPlayerIndicator: currentPlayerIndicatorImageMap,
		StackSizeFont:          stackSizeFont,
		ResultFont:             resultFont,
	}
}

func (manager *Manager) ResetBoardImage() {
	boardImage, _, err := ebitenutil.NewImageFromFile("assets/images/DVONN-board.png")
	if err != nil {
		log.Fatal(err)
	}
	manager.BoardImage = boardImage
}
