package main

import (
	"log"
	"mirtilo-dvonn/internal/game"
	"mirtilo-dvonn/internal/graphics"
	"mirtilo-dvonn/internal/model"
	"mirtilo-dvonn/internal/protocol"
	"net"
	"sync"

	"github.com/hajimehoshi/ebiten/v2"
)

type Client struct {
	game              *game.Game
	playerColor       model.PieceColor
	currentPlayerMove model.PieceColor
	conn              *net.TCPConn
}

var wg = sync.WaitGroup{}

func main() {
	tcpAddr, err := net.ResolveTCPAddr(protocol.ConnType, protocol.ConnHost+":"+protocol.ConnPort)

	if err != nil {
		log.Fatal("ResolveTCPAddr failed:", err.Error())
	}

	conn, err := net.DialTCP(protocol.ConnType, nil, tcpAddr)
	if err != nil {
		log.Fatal("Dial failed:", err.Error())
	}

	defer func() {
		err = conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	client := Client{game: game.NewGameClient(), conn: conn, currentPlayerMove: model.White}

	_, err = conn.Write([]byte{protocol.ColorAssignment})
	if err != nil {
		log.Fatal("Write to server failed:", err.Error())
	}

	reply := make([]byte, 1)
	_, err = conn.Read(reply)

	if err != nil {
		log.Fatal("Write to server failed:", err.Error())
	}

	if reply[0] == byte(model.White) || reply[0] == byte(model.Black) {
		client.playerColor = model.PieceColor(reply[0])
	} else if reply[0] == byte(model.Empty) {
		log.Fatal("cannot connect, two players already on server")
	} else {
		log.Fatal("given color must be white or black")
	}

	wg.Add(3)

	go client.handleTileClicked()
	go client.handleGameRestart()
	go client.handleServerConn()

	ebiten.SetWindowSize(graphics.ScreenWidth, graphics.ScreenHeight)
	ebiten.SetWindowTitle("Mirtilo DVONN")

	if err := ebiten.RunGame(client.game); err != nil {
		log.Fatal(err)
	}

	wg.Wait()
}

func (c *Client) handleTileClicked() {
	for clickedIdx := range c.game.UIChannels.TileClickedChannel {
		if c.playerColor == c.currentPlayerMove {
			tileClickedRequest := []byte{protocol.TileClicked, byte(clickedIdx[0]), byte(clickedIdx[1])}
			_, err := c.conn.Write(tileClickedRequest)

			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func (c *Client) handleGameRestart() {
	for range c.game.UIChannels.GameRestartChannel {
		_, err := c.conn.Write([]byte{protocol.GameRestartClicked})
		if err != nil {
			log.Fatal(err)
		}
	}
}

func (c *Client) handleServerConn() {
	defer wg.Done()

	for {
		reply := make([]byte, 512)
		_, err := c.conn.Read(reply)

		if err != nil {
			log.Fatal(err)
		}

		readCount := reply[0]
		replyType := reply[1]

		switch replyType {
		case protocol.PiecePlaced:
			tilePlacedIdx := [2]int8{int8(reply[2]), int8(reply[3])}
			color := model.PieceColor(reply[4])
			c.game.LogicChannels.PiecePlacedChannel <- model.PiecePlaced{Pos: tilePlacedIdx, Color: color}
		case protocol.UpdatePlayer:
			curPlayer := model.PieceColor(reply[2])
			c.currentPlayerMove = curPlayer
			c.game.LogicChannels.UpdateCurrentPlayerChannel <- curPlayer
		case protocol.PieceSelected:
			posIdx := [2]int8{int8(reply[2]), int8(reply[3])}

			var dests [][2]int8

			for i := uint8(4); i < readCount; i += 2 {
				dests = append(dests, [2]int8{int8(reply[i]), int8(reply[i+1])})
			}
			c.game.LogicChannels.PieceSelectedChannel <- model.PieceSelected{Pos: posIdx, Dests: dests}
		case protocol.PieceUnselected:
			c.game.LogicChannels.PieceUnselectedChannel <- struct{}{}
		case protocol.PieceMoved:
			c.game.LogicChannels.PieceMovedChannel <- model.PieceMoved{
				OriginPos: [2]int8{int8(reply[2]), int8(reply[3])},
				DestPos:   [2]int8{int8(reply[4]), int8(reply[5])},
			}
		case protocol.RemovePieces:
			var toRemove [][2]int8
			for i := uint8(2); i < readCount; i += 2 {
				toRemove = append(toRemove, [2]int8{int8(reply[i]), int8(reply[i+1])})
			}
			c.game.LogicChannels.RemoveSectionsChannel <- toRemove
		case protocol.GameOver:
			c.game.LogicChannels.GameOverChannel <- struct{}{}
		case protocol.GameRestart:
			c.game.LogicChannels.GameRestartChannel <- struct{}{}
		case protocol.GamePaused:
			c.game.LogicChannels.GamePausedChannel <- struct{}{}
		}
	}
}
