package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"log"
	mirtiloDVONN "mirtilo-dvonn/internal/game"
	"mirtilo-dvonn/internal/graphics"
)

func main() {
	game := mirtiloDVONN.NewGameStandalone()

	ebiten.SetWindowSize(graphics.ScreenWidth, graphics.ScreenHeight)
	ebiten.SetWindowTitle("Mirtilo DVONN")

	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}
