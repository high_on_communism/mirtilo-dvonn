package main

import (
	"log"
	"mirtilo-dvonn/internal/game"
	"mirtilo-dvonn/internal/model"
	"mirtilo-dvonn/internal/protocol"
	"net"
	"sync"
)

type Server struct {
	game         *game.Game
	player1Color model.PieceColor
	player2Color model.PieceColor
	listener     net.Listener
	conn1        net.Conn
	conn2        net.Conn
	readChannel  chan []byte
	writeChannel chan []byte
}

var wg = sync.WaitGroup{}

func main() {
	// Listen for incoming connections.
	l, err := net.Listen(protocol.ConnType, protocol.ConnHost+":"+protocol.ConnPort)
	if err != nil {
		log.Fatal("Error listening:", err.Error())
	}
	// Close the listener when the application closes.
	defer func(l net.Listener) {
		err := l.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(l)

	server := Server{game: game.NewGameServer(),
		player1Color: model.Empty, player2Color: model.Empty, listener: l, conn1: nil, conn2: nil,
		readChannel: make(chan []byte), writeChannel: make(chan []byte)}

	wg.Add(10)

	go server.handleConn()

	go server.writeHandler()

	go server.piecePlacedHandler()
	go server.updateCurrentPlayerHandler()
	go server.pieceSelectedHandler()
	go server.pieceUnselectedHandler()
	go server.pieceMovedHandler()
	go server.removeSectionsHandler()
	go server.gameOverHandler()
	go server.gameRestartHandler()

	wg.Wait()
}

func (s *Server) handleConn() {
	defer wg.Done()

	for {
		// Listen for an incoming connection.
		conn, err := s.listener.Accept()
		if err != nil {
			log.Println("Error accepting: ", err.Error())
		} else {
			go s.handleRequest(conn)
		}
	}
}

// Handles incoming requests.
func (s *Server) handleRequest(conn net.Conn) {
	for {
		// Make a buffer to hold incoming data.
		buf := make([]byte, 1024)
		// Read the incoming connection into the buffer.
		_, err := conn.Read(buf)
		if err != nil {
			log.Println("Error reading:", err.Error())
		}

		switch buf[0] {
		case protocol.ColorAssignment: // handle color assignment
			s.colorAssignment(conn)
		case protocol.TileClicked: // tile clicked
			clickedIdx := [2]int8{int8(buf[1]), int8(buf[2])}
			s.game.UIChannels.TileClickedChannel <- clickedIdx
		case protocol.GameRestartClicked: // game restart
			s.game.UIChannels.GameRestartChannel <- struct{}{}
			s.game.LogicChannels.GameRestartChannel <- struct{}{}
		}
	}
}

func (s *Server) colorAssignment(conn net.Conn) {
	if s.player1Color == model.Empty {
		s.player1Color = model.White
		conn.Write([]byte{byte(model.White)})
		s.conn1 = conn
	} else if s.player2Color == model.Empty {
		s.player2Color = model.Black
		conn.Write([]byte{byte(model.Black)})
		s.conn2 = conn
	} else {
		conn.Write([]byte{byte(model.Empty)})
		conn.Close()
	}
}

func (s *Server) readHandler(conn net.Conn) {
}

func (s *Server) writeHandler() {
	for bytes := range s.writeChannel {
		for _, conn := range [2]net.Conn{s.conn1, s.conn2} {
			_, err := conn.Write(bytes)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func (s *Server) piecePlacedHandler() {
	defer wg.Done()

	for piecePlaced := range s.game.LogicChannels.PiecePlacedChannel {
		tilePlacedBytes := []byte{4, protocol.PiecePlaced, byte(piecePlaced.Pos[0]), byte(piecePlaced.Pos[1]), byte(piecePlaced.Color)}
		s.writeChannel <- tilePlacedBytes
	}
}

func (s *Server) updateCurrentPlayerHandler() {
	defer wg.Done()

	for currentPlayer := range s.game.LogicChannels.UpdateCurrentPlayerChannel {
		s.writeChannel <- []byte{2, protocol.UpdatePlayer, byte(currentPlayer)}
	}
}

func (s *Server) pieceSelectedHandler() {
	defer wg.Done()

	for pieceSelected := range s.game.LogicChannels.PieceSelectedChannel {
		res := []byte{byte(3 + 2*int8(len(pieceSelected.Dests))), protocol.PieceSelected,
			byte(pieceSelected.Pos[0]), byte(pieceSelected.Pos[1])}
		for _, destPos := range pieceSelected.Dests {
			res = append(res, byte(destPos[0]), byte(destPos[1]))
		}

		s.writeChannel <- res
	}
}

func (s *Server) pieceUnselectedHandler() {
	defer wg.Done()

	for range s.game.LogicChannels.PieceUnselectedChannel {
		s.writeChannel <- []byte{1, protocol.PieceUnselected}
	}
}

func (s *Server) pieceMovedHandler() {
	defer wg.Done()

	for pieceMoved := range s.game.LogicChannels.PieceMovedChannel {
		s.writeChannel <- []byte{5, protocol.PieceMoved,
			byte(pieceMoved.OriginPos[0]), byte(pieceMoved.OriginPos[1]),
			byte(pieceMoved.DestPos[0]), byte(pieceMoved.DestPos[1])}
	}
}

func (s *Server) removeSectionsHandler() {
	defer wg.Done()

	for removeSections := range s.game.LogicChannels.RemoveSectionsChannel {
		var toRemoveTiles []byte

		count := byte(1)

		for _, t := range removeSections {
			toRemoveTiles = append(toRemoveTiles, byte(t[0]), byte(t[1]))
			count += 2
		}

		s.writeChannel <- append([]byte{count, protocol.RemovePieces}, toRemoveTiles...)
	}
}

func (s *Server) gameOverHandler() {
	defer wg.Done()

	for range s.game.LogicChannels.GameOverChannel {
		s.writeChannel <- []byte{1, protocol.GameOver}
	}
}

func (s *Server) gameRestartHandler() {
	defer wg.Done()

	for range s.game.LogicChannels.GameRestartChannel {
		s.writeChannel <- []byte{1, protocol.GameRestart}
	}
}

func (s *Server) gamePausedHandler() {
	defer wg.Done()

	for range s.game.LogicChannels.GamePausedChannel {
		s.writeChannel <- []byte{1, protocol.GamePaused}
	}
}
